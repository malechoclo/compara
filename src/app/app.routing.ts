import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component'
const APP_ROUTES: Routes = [
  { path: '', component: HomeComponent },
  // { path: 'dash', component: DashComponent ,canActivate:[AuthGuardService]},
  // { path: 'ordenes', component: OrdenesComponent ,canActivate:[AuthGuardService]},
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
