import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

/* COMPONENT */
import { HomeComponent } from './components/home/home.component';

//ROUTING
import {APP_ROUTING} from './app.routing';

/* UTILITIES */
import { MaterialModule } from './modules/material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


/* SERVICES */
import { CarInsuranceService } from './services/car-insurance.service';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    APP_ROUTING,
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [
    CarInsuranceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
