import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as _ from 'lodash';
import { CarInsuranceService } from '../../services/car-insurance.service';
import { Product } from '../../interface/products';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false
})
export class HomeComponent implements OnInit {
  precios: any = [];
  updatedPrices: any = [];
  diasMenos: number = 0;
  private productsAtDayZeroDefault: Array<Product> = [
    new Product('Full Coverage', 2, 0),
    new Product('Full Coverage', 8, 30),
    new Product('Special Full Coverage', 15, 20),
    new Product('Special Full Coverage', 10, 49),
    new Product('Special Full Coverage', 5, 49),
    new Product('Super Sale', 10, 16),
		new Product('Mega Coverage', 10, 80),
		new Product('Mega Coverage', 3, 80),
    new Product('Super Sale', 15, 23),
    new Product('Super Sale', 5, 30),
		new Product('Medium Coverage', 10, 20),
		new Product('Low Coverage', 5, 7)
  ];

  private productsAtDayZero: Array<Product> = [];
  constructor(public ci: CarInsuranceService) {
    this.productsAtDayZero = this.productsAtDayZeroDefault;
    this.ci.init(this.productsAtDayZero);
  }

  ngOnInit() {

  }

  decreceDays() {
    this.ci.decreceDays().subscribe(data => {
      this.updatedPrices = data;
      this.diasMenos--;
    });
  }

  resetProduct() {
    console.log("voy a re iniciar los productos", this.productsAtDayZeroDefault);
    this.productsAtDayZero = [];
    this.productsAtDayZero = this.productsAtDayZeroDefault;
    this.ci.init(this.productsAtDayZero);
  }

}
