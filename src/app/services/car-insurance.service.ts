import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../interface/products';
@Injectable()
export class CarInsuranceService {
  products: Array<Product> = [];
  constructor() {
    
  }

  init(products: Array<Product>) {
    this.products = products;
    console.log("productos en servicio", this.products);
  }

  rules(p) {
    switch (p.name) {
      case 'Full Coverage':
        console.log('Full Coverage')
        // actually increases in price the older it gets.
        p.sellIn = p.sellIn - 1;
        if (p.price <= 50) {
          p.price = p.price + 1;
        }
        break;
      case 'Mega Coverage':
        // being a legendary product, never has to be sold or decreases in price.
        console.log('Medium Coverage')
        break;
      case 'Special Full Coverage':
        // like full coverage, increases in price as its sellIn value approaches:
        console.log('Special Full Coverage')
        p.sellIn = p.sellIn - 1;
        if (p.price <= 50) {
          if (p.sellIn === 0) {
            p.price = p.sellIn;
          } else if (p.sellIn > 10) {
            p.price = p.price + 1;
          }
          else if (p.sellIn <= 10 && p.sellIn > 5) {
            p.price = p.price + 2;
          } else if (p.sellIn <= 5 && p.sellIn > 0) {
            p.price = p.price + 3;
          }
        }
        break;
      case 'Super Sale':
        console.log('Super Sale')
        // Products degrade in price twice as fast as normal Products.
        p.sellIn = p.sellIn - 1;
        if (p.price > 1) {
          if (p.sellIn > 0) {
            p.price = p.price - 2;
          }
        }
        break;
      default:
        console.log('other')
    }
  }

  decreceDays() {
    return Observable.create(observer => {
      observer.next(
        this.products.forEach((e) => {
          if (e.sellIn > 0) {
            this.rules(e);
          }
        })
      );
      observer.complete();
    });
  }
}
